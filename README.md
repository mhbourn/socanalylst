# README #

### Setup ###
1. Get your API tokens from the different providers.  They're free and the accounts are easy to set up.
2. Enter your tokens in the appropriate spots in socVars.py
3. Install hashid: pip install hashid
4. Place the files together somewhere
5. In Linux, I recomment the following
    1. Place the files in: ~/dev/soc
    2. Create a soft link to soc.py in ~/bin so that it can be called from anywhere in the terminal

* I'm not a developer, I will not be maintaining this, you're on your own
* There's a lot of data returned from the APIs that doesn't get used in the current setup
    * You may want to explore the data and tailor the output to your own tastes
--------------------------------------------------------------------------------------------------------
### Get your API creds from these OSINT sources ###

* [https://www.ioclists.com/#api]
* [https://www.phishtank.com/developer_info.php]
* [https://checkphish.ai/checkphish-api/]
* [https://whois.whoisxmlapi.com/documentation/making-requests/]
* [https://www.ipqualityscore.com/documentation/overview/]
* [https://getipintel.net/]
    * You don't need to sign up, just use an email address in the query
* [https://bazaar.abuse.ch/api/]
* [https://malshare.com/doc.php]
* [https://developers.virustotal.com/v3.0/reference]
* [https://www.projecthoneypot.org/httpbl_api.php]
* [https://docs.dnsdb.info/dnsdb-apiv2/]
* [https://analyze.intezer.com/]
* [https://www.hybrid-analysis.com/docs/api/v2]
* [https://onionoo.torproject.org/]
    * No sign up necessary
--------------------------------------------------------------------------------------------------------
### Files ###

* soc.py
    * The main executable
    
* socFuncsDoms.py
    * Contains the functions for working with domains

* socFuncsHashs.py
    * Contains the functions for working with hashes

* socFuncsIPs.py
    * Contains the functions for working with IPs

* socLists.py
    * Contains lists used by various functions

* socVars.py
    * Contains the variables used by the functions

* README
    * Contains this ... oh noes, infinite recursion ...
--------------------------------------------------------------------------------------------------------





