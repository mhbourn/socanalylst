# By: Matthew Bourn
# On: 2020-10-20
# ©: GNU GPLv3
# Do: at your own risk; copy, modify, share, give me a nod.  Don't sell.

###################################################################################################
# Project HoneyPot Score Translations
hpMeanings = [(0,"Search Engine (0)"),(1,"Suspicious (1)"),(2,"Harvester (2)"),(3,"Suspicious & Harvester (1+2)"),(4,"Comment Spammer (4)"),(5,"Suspicious & Comment Spammer (1+4)"),(6,"Harvester & Comment Spammer (2+4)"),(7,"Suspicious & Harvester & Comment Spammer (1+2+4)")]

###################################################################################################
# IP Intel Error Code Translations
iiErrs = ["Invalid/no input","Invalid IP","Private IP","DB unavailable","Request originating from banned IP","No email address included"]

